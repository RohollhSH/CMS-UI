<?php include 'header-navbar-sidebar.php'; ?>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-5">
				<div class="card card-plain">
					<div class="card-header card-header-primary">
						<h4 class="card-title mt-0"> جدول برچسب ها</h4>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead class="text-muted">
								<th>
									<a href="#" class="text-info">شماره</a>
								</th>
								<th>
									<a href="#" class="text-info">نام</a>
								</th>
								<th>
									عملیات
								</th>

								</thead>
								<tbody>
								<tr>
									<td>
										1
									</td>
									<td>
										Dakota Rice
									</td>
									<td class="text-primary">
										<a href="#" data-toggle="tooltip" title="حذف" class="material-icons">delete</a>
										<a href="#" data-toggle="tooltip" title="ویرایش" class="material-icons">edit</a>
									</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-7">
				<div class="card">
					<div class="card-header card-header-primary">
						<h4 class="card-title">ایجاد دسته</h4>
					</div>
					<div class="card-body">
						<form>
							<div class="row">
								<div class="col-md-8">
									<div class="form-group">
										<label class="bmd-label-floating">عنوان دسته</label>
										<input type="text" class="form-control">
									</div>
								</div>
							</div>

							<button type="submit" class="btn btn-primary pull-right">اعمال تغییرات</button>
							<div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.min.js" type="text/javascript"></script>
<script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="../assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chartist JS -->
<script src="../assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
</body>

</html>
