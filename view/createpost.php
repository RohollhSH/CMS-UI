<?php include 'header-navbar-sidebar.php';?>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">ایجاد پست</h4>
                            </div>
                            <div class="card-body">
                                <form>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">عنوان پست</label>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                             <div class="form-group">
                                                 <label for="comment" class="bmd-label-floating">محتوی</label>
                                                 <textarea class="form-control" rows="10" id="comment"></textarea>
                                           </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="bmd-label-floating"> عکس پست</label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="btn btn-primary pull-right">
                                                    جستجو <input type="file" hidden>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">وضعیت نوشته
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a class="dropdown-item" href="#">پیش نویس</a></li>
                                                        <li><a class="dropdown-item active" href="#">آماده ارسال</a></li>
                                                        <li><a class="dropdown-item" href="#">ارسال</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">

                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating">دسته بندی</label>
                                                    </div>
                                                </div>

                                                <label class="container">دسته اول
                                                    <input type="checkbox" checked="checked">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="container">دسته دوم
                                                    <input type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="container">دسته سوم
                                                    <input type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="container">دسته چهارم
                                                    <input type="checkbox">
                                                    <span class="checkmark"></span>
                                                </label>

                                            </div>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-primary pull-right">اعمال تغییرات</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-profile">
                            <div class="card-avatar">
                                <a href="#pablo">
                                    <img class="img" src="../assets/img/faces/robot.jpg"/>
                                </a>
                            </div>
                            <div class="card-body">
                                <h6 class="card-category text-gray">مدیر عامل / مدیر فنی</h6>
                                <h4 class="card-title">روح اله شهبازی</h4>
                                <p class="card-description">
                                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان
                                    گرافیک است.
                                </p>
                                <a href="#pablo" class="btn btn-primary btn-round">دنبال کردن</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <nav class="float-left">
                    <ul>
                        <li>
                            <a href="https://www.creative-tim.com">
                                تیم خلاق
                            </a>
                        </li>
                        <li>
                            <a href="https://creative-tim.com/presentation">
                                درباره ما
                            </a>
                        </li>
                        <li>
                            <a href="http://blog.creative-tim.com">
                                بلاگ
                            </a>
                        </li>
                        <li>
                            <a href="https://www.creative-tim.com/license">
                                اجازه نامه
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright float-right">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>
                    , ساخته شده با
                    <i class="material-icons">favorite</i> توسط
                    <a href="https://www.creative-tim.com" target="_blank">تیم خلاق</a> برای وب بهتر.
                </div>
            </div>
        </footer>
    </div>
</div>
<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.min.js" type="text/javascript"></script>
<script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="../assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chartist JS -->
<script src="../assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, do
n't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
</body>

</html>
