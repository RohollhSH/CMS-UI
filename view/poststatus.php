<?php include "header-navbar-sidebar.php"; ?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">جدول نوشتارها</h4>
                        <p class="card-category">بمنظور مرتب سازی جدول بر اساس یک ستون خاص بر روی عنوان ستون کلیک کنید</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=text-muted>
                                <th>
                                    <a href="#" class="text-info">شماره</a>
                                </th>
                                <th>
                                    <a href="#" class="text-info">عنوان</a>
                                </th>
                                <th>
                                    نوشته
                                </th>
                                <th>
                                    <a href="#" class="text-info">نویسنده</a>
                                </th>
                                <th>
                                    <a href="#" class="text-info">تاریخ</a>
                                </th>
                                <th>
                                    عملیات
                                </th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        1
                                    </td>
                                    <td>
                                        چگونه طراحی کنیم
                                    </td>
                                    <td>
                                        طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای...
                                    </td>
                                    <td>
                                        حسین محمودی
                                    </td>
                                    <td>
                                        97/5/26
                                    </td>
                                    <td class="text-primary">
                                        <a href="#" data-toggle="tooltip" title="حذف" class="material-icons">delete</a>
                                        <a href="#" data-toggle="tooltip" title="ویرایش" class="material-icons">edit</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        2
                                    </td>
                                    <td>
                                        چگونه بنویسیم
                                    </td>
                                    <td>
                                        طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای...

                                    </td>
                                    <td>
                                        وحید سلطانی
                                    </td>
                                    <td>
                                        97/5/26
                                    </td>
                                    <td class="text-primary">
                                        <a href="#" data-toggle="tooltip" title="حذف" class="material-icons">delete</a>
                                        <a href="#" data-toggle="tooltip" title="ویرایش" class="material-icons">edit</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
            <!--   Core JS Files   -->
            <script src="../assets/js/core/jquery.min.js" type="text/javascript"></script>
            <script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
            <script src="../assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
            <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
            <!--  Google Maps Plugin    -->
            <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
            <!-- Chartist JS -->
            <script src="../assets/js/plugins/chartist.min.js"></script>
            <!--  Notifications Plugin    -->
            <script src="../assets/js/plugins/bootstrap-notify.js"></script>
            <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
            <script src="../assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
            <!-- Material Dashboard DEMO methods, don't include it in your project! -->
            <script src="../assets/demo/demo.js"></script>
            <script>
                $(document).ready(function () {
                    // Javascript method's body can be found in assets/js/demos.js
                    md.initDashboardPageCharts();

                });
            </script>
            </body>

            </html>