<?php include 'header-navbar-sidebar.php'; ?>


<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title mt-0"> جدول کاربران</h4>
                        <p class="card-category"> بمنظور مرتب سازی جدول بر اساس یک ستون خاص بر روی عنوان ستون کلیک کنید</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="text-muted">
                                <th>
                                    <a href="#" class="text-info">شماره</a>
                                </th>
                                <th>
                                    <a href="#" class="text-info">نام</a>
                                </th>
                                <th>
                                    <a href="#" class="text-info">کشور</a>
                                </th>
                                <th>
                                    <a href="#" class="text-info">شهر</a>
                                </th>
                                <th>
                                    <a href="#" class="text-info">رنگ کاربر</a>
                                </th>
                                <th>
                                    هزینه ی پرداختی توسط کاربر
                                </th>
                                <th>
                                    هزینه ی پرداختی توسط مرجوعان کاربر
                                </th>
                                <th>
                                    <a href="#" class="text-info">تعداد نوشته</a>
                                </th>
                                <th>
                                    عملیات
                                </th>

                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        1
                                    </td>
                                    <td>
                                        Dakota Rice
                                    </td>
                                    <td>
                                        Niger
                                    </td>
                                    <td>
                                        Oud-Turnhout
                                    </td>
                                    <td>
                                        طلایی
                                    </td>
                                    <td>
                                        5000
                                    </td>
                                    <td>
                                        0
                                    </td>
                                    <td>
                                        3
                                    </td>
                                    <td class="text-primary">
                                        <a href="#" data-toggle="tooltip" title="حذف" class="material-icons">delete</a>
                                        <a href="#" data-toggle="tooltip" title="ویرایش" class="material-icons">edit</a>
                                        <a href="#" data-toggle="tooltip" title="پیام کاربر به شما" class="material-icons">mail</a>
                                        <a href="#" data-toggle="tooltip" title="نوشتار های کاربر" class="material-icons">border_color</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        2
                                    </td>
                                    <td>
                                        Minerva Hooper
                                    </td>
                                    <td>
                                        Curaçao
                                    </td>
                                    <td>
                                        Sinaai-Waas
                                    </td>
                                    <td>
                                        $23,789
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        3
                                    </td>
                                    <td>
                                        Sage Rodriguez
                                    </td>
                                    <td>
                                        Netherlands
                                    </td>
                                    <td>
                                        Baileux
                                    </td>
                                    <td>
                                        $56,142
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        4
                                    </td>
                                    <td>
                                        Philip Chaney
                                    </td>
                                    <td>
                                        Korea, South
                                    </td>
                                    <td>
                                        Overland Park
                                    </td>
                                    <td>
                                        $38,735
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        5
                                    </td>
                                    <td>
                                        Doris Greene
                                    </td>
                                    <td>
                                        Malawi
                                    </td>
                                    <td>
                                        Feldkirchen in Kärnten
                                    </td>
                                    <td>
                                        $63,542
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        6
                                    </td>
                                    <td>
                                        Mason Porter
                                    </td>
                                    <td>
                                        Chile
                                    </td>
                                    <td>
                                        Gloucester
                                    </td>
                                    <td>
                                        $78,615
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="container-fluid">
        <nav class="float-left">
            <ul>
                <li>
                    <a href="https://www.creative-tim.com">
                        تیم خلاق
                    </a>
                </li>
                <li>
                    <a href="https://creative-tim.com/presentation">
                        درباره ما
                    </a>
                </li>
                <li>
                    <a href="http://blog.creative-tim.com">
                        بلاگ
                    </a>
                </li>
                <li>
                    <a href="https://www.creative-tim.com/license">
                        اجازه نامه
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright float-right">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script>
            , ساخته شده با
            <i class="material-icons">favorite</i> توسط
            <a href="https://www.creative-tim.com" target="_blank">تیم خلاق</a> برای وب بهتر.
        </div>
    </div>
</footer>
</div>
</div>
<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.min.js" type="text/javascript"></script>
<script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="../assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chartist JS -->
<script src="../assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
</body>

</html>